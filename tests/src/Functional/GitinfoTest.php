<?php

namespace Drupal\Tests\gitinfo\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group gitinfo
 */
class GitinfoTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['gitinfo', 'dblog'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'access site reports',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Test some pages.
   */
  public function testLoad() {
    // Test that the home page loads with a 200 response.
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);

    // Visit the Status report page that requires login with a url object.
    $url = Url::fromRoute('system.status');
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);

    // Test that the page contains the Git info section.
    $this->assertSession()->pageTextContains('Git info');

    // Visit the Gitinfo report page that requires login with a url object.
    $url = Url::fromRoute('gitinfo.report');
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);

    // Test that the page contains the Git info page title.
    $this->assertSession()->pageTextContains('Git info');

    // Visit the Gitinfo report page that requires login with a url object.
    $url = Url::fromRoute('dblog.overview');
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);

    // Test that the page contains the gitinfo event.
    $this->assertSession()->pageTextContains('gitinfo');
  }

  /**
   * Test form submission.
   */
  public function testFormSubmission() {
    // Get form and check it loads correctly.
    $url = Url::fromRoute('gitinfo.settings');
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertText('Connection with the remote repos');
    // Empty form submission. Check presence of the default value.
    $this->drupalPostForm(NULL, [], 'Save configuration');
    $this->assertText('vendor');
    // Form validation testing of a non-existing directory rejection.
    $edit = ['extra_directories' => '/non-existing/directory'];
    $this->drupalPostForm(NULL, $edit, 'Save configuration');
    $this->assertText('Directory /non-existing/directory does not exist.');
    // Form validation testing of an invalid characters rejection.
    $edit = ['extra_directories' => '', 'aggregate_directories' => 'some/invalid/ch@r@ter$'];
    $this->drupalPostForm(NULL, $edit, 'Save configuration');
    $this->assertText('Directory some/invalid/ch@r@ter$ is invalid.');
    // Form validation testing of an invalid date format.
    $edit = [
      'extra_directories' => '',
      'aggregate_directories' => '',
      'exact_date_format' => 'l \t\h\e jS \of F Y h:i:s Agibberish',
    ];
    $this->drupalPostForm(NULL, $edit, 'Save configuration');
    $this->assertText('Invalid format.');
    // A correct form submission.
    $edit = [
      'extra_directories' => '',
      'aggregate_directories' => 'vendor',
      'number_commit_messages' => 0,
      'fuzzy_timestamp_granularity' => 9,
      'exact_date_format' => 'l \t\h\e jS \of F Y h:i:s A',
    ];
    $this->drupalPostForm(NULL, $edit, 'Save configuration');
    $this->assertText('The configuration options have been saved.');
  }

}
