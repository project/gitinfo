<?php

/**
 * @file
 * Install requirements for the Git Info Report module.
 */

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function gitinfo_requirements($phase) {
  // Route name is added to not impact performance outside the report itself.
  $routeName = \Drupal::routeMatch()->getRouteName();
  if ($phase === 'install' || ($phase === 'runtime' && $routeName === 'system.status')) {
    // Initialize some variables.
    $requirements = [];
    $severity = REQUIREMENT_OK;
    $gitInfoModule = Link::fromTextAndUrl(t('Git Info Report'), Url::fromRoute('system.modules_uninstall', [], [
      'attributes' => [
        'title' => t('Uninstall'),
      ],
    ]))->toString();
    // Check the internet connection being available to check remotes.
    $noConnection = NULL;
    // Turn on output buffering (ob) to avoid display of command output.
    ob_start();
    passthru("ping -c 1 google.com", $noConnection);
    ob_end_clean();
    // Track if an error message already kicked in to avoid being overwritten.
    $error = FALSE;
    // Generate a Status report error message if shell_exec is not available.
    if (!gitinfo_function_enabled('shell_exec')) {
      $shellExec = Link::fromTextAndUrl(t('shell_exec'), Url::fromUri('https://www.php.net/manual/en/function.shell-exec.php', [
        'attributes' => [
          'title' => t('PHP: shell_exec - Manual'),
          'target' => '_blank',
        ],
      ]))->toString();
      $disableFunctions = Link::fromTextAndUrl(t('disable_functions'), Url::fromUri('https://www.php.net/manual/en/ini.core.php#ini.disable-functions', [
        'attributes' => [
          'title' => t('PHP: Description of core php.ini directives - Manual'),
          'target' => '_blank',
        ],
      ]))->toString();
      $value = t("Remove the PHP function @shell_exec from the @disable_functions directive in the PHP.INI settings or uninstall the @gitinfo module.", [
        '@shell_exec' => $shellExec,
        '@disable_functions' => $disableFunctions,
        '@gitinfo' => $gitInfoModule,
      ]);
      $severity = REQUIREMENT_ERROR;
      $error = TRUE;
    }
    // Detection method for a non-Unix OS. 'which' command is typical for Unix.
    elseif (!(strpos(shell_exec('which find'), '/find') !== FALSE)) {
      $value = t("The terminal command 'find' is not available. The server's OS is probably non-Unix. Uninstall the @gitinfo module.", ['@gitinfo' => $gitInfoModule]);
      $severity = REQUIREMENT_ERROR;
      $error = TRUE;
    }
    else {
      // Git version info.
      $version = trim(shell_exec('git --version'));
      $latestVersion = $noConnection ? NULL : trim(shell_exec('git ls-remote --tags --sort="v:refname" git://github.com/git/git.git | tail | sed \'s/.*\/v//; s/\^{}//; s/^[0-9].\+-rc[0-9]//; /^$/d;\' | tail -n1'));
      if ($version == 'git version ' . $latestVersion) {
        $latestVersion = t('<strong>Up to date</strong>.');
      }
      elseif ($noConnection) {
        $severity = REQUIREMENT_WARNING;
        $latestVersion = t('No internet connection available to check the latest version.');
      }
      else {
        $severity = REQUIREMENT_WARNING;
        $update = Link::fromTextAndUrl(t('update'), Url::fromUri('https://unix.stackexchange.com/a/170831/139407', [
          'attributes' => [
            'title' => t('Update to a newer version of Git using apt-get - Stack Exchange'),
            'target' => '_blank',
          ],
        ]))->toString();
        $latestVersion = Link::fromTextAndUrl(t($latestVersion), Url::fromUri('https://git-scm.com/downloads', [
          'attributes' => [
            'title' => t('Git - Downloads'),
            'target' => '_blank',
          ],
        ]))->toString();
        $latestVersion = t('<strong>Latest stable version available @latestversion</strong>. Consider to @update.', ['@latestversion' => $latestVersion, '@update' => $update]);
      }
      // User info.
      $user = trim(shell_exec('whoami'));
      $otherUser = trim(strstr(shell_exec('who -q'), PHP_EOL, TRUE));
      // Only note another user if different.
      $otherUser = $user != $otherUser ? t('Not as <strong>%otheruser</strong>.', ['%otheruser' => $otherUser]) : NULL;
      $user_info = t("PHP shell commands, including <em>git</em>, are executed as user <strong>%user</strong>. @otheruser That is normal. If any action is needed, it will be pointed out in the status messages of the detailed report.", [
        '%user' => $user,
        '@otheruser' => $otherUser,
      ]);
      $whichGit = preg_replace('/\s+/', '', shell_exec('which git'));
    }
    // Generate a Status report error message if Git is not installed ..
    if (!$error && strpos($version, 'not found') !== FALSE) {
      $value = t("Git not installed. Install Git or uninstall the @gitinfo module.", ['@gitinfo' => $gitInfoModule]);
      $severity = REQUIREMENT_ERROR;
      $error = TRUE;
    }
    // .. or add the Git version info to the output.
    elseif (!$error) {
      $value = t('@version. @latestversion Located at: %location<br />@user_info', [
        '@version' => ucfirst(trim($version)),
        '@latestversion' => $latestVersion,
        '%location' => $whichGit,
        '@user_info' => $user_info,
      ],
      );
    }
    if ($phase === 'runtime') {
      // Get data of the latest generated report.
      $keys = [
        'gitinfo.generated',
        'gitinfo.severity',
        'gitinfo.etime',
        'gitinfo.connect',
        'gitinfo.connection',
        'gitinfo.monitored',
      ];
      $data = \Drupal::state()->getMultiple($keys);
      // Add labels and format.
      $items = [
        'generated' => isset($data['gitinfo.generated']) ? t('Generated %generated ago for %monitored directories in %etime', [
          '%generated' => \Drupal::service('date.formatter')->formatInterval(time() - $data['gitinfo.generated'], 2),
          '%monitored' => $data['gitinfo.monitored'],
          '%etime' => \Drupal::service('date.formatter')->formatInterval($data['gitinfo.etime'], 2),
        ]) : t('Not yet generated'),
        'severity' => isset($data['gitinfo.severity']) ? t('Severity: %severity', ['%severity' => $data['gitinfo.severity']]) : NULL,
        'connect' => isset($data['gitinfo.connection']) ? t('Remote connection: %connect, @connection', [
          '%connect' => empty($data['gitinfo.connect']) ? t('none') : $data['gitinfo.connect'],
          '@connection' => $data['gitinfo.connection'] ? t('was successfully established') : t('was not established'),
        ]) : NULL,
      ];
      // All screen messages as shown on the detailed report.
      $ops = ['headfix', 'fetch', 'pull'];
      $types = ['warning', 'error', 'status'];
      foreach ($ops as $op) {
        foreach ($types as $type) {
          $message = \Drupal::cache()->get('gitinfo.' . $op . '.' . $type . '.message');
          if ($message) {
            $items[] = t('@type - @message', ['@type' => strtoupper($type), '@message' => $message->data]);
          }
        }
      }
      // Link to the database log page filtered for 'gitinfo'.
      $dblogLink = NULL;
      if (\Drupal::moduleHandler()->moduleExists('dblog')) {
        $dblogLink = Link::fromTextAndUrl(t("Recent log messages"), Url::fromRoute('dblog.overview', ['type' => ['gitinfo']], [
          'attributes' => [
            'title' => t('dblog'),
          ],
        ]))->toString();
      }
      // Link to the detailed report.
      $moreInfoLink = Link::fromTextAndUrl(t("View the detailed report"), Url::fromRoute('gitinfo.report', [], [
        'attributes' => [
          'title' => t('Git info'),
        ],
      ]))->toString();
      $items = array_merge($items, [
        'dblog' => t('See %dbloglink for details', ['%dbloglink' => $dblogLink]),
        'report' => $moreInfoLink,
      ]);
      // Set severity in correspondence with the report but do not lower it.
      $errorReport = NULL;
      $last = t('last');
      if (isset($data['gitinfo.severity'])) {
        switch ($data['gitinfo.severity']) {
          case 'error':
            $severity = REQUIREMENT_ERROR;
            $errorReport = [
              '#markup' => t('<p><strong>Last detailed report could not be generated.</strong></p>'),
            ];
            $last = t('before last');
            break;

          case 'warning':
            $severity = $severity == REQUIREMENT_ERROR ? $severity : REQUIREMENT_WARNING;
            break;
        }
      }
      $items = is_array($items) ? array_filter($items) : NULL;
      $lastReport = $items ? [
        '#title' => t('Metadata of the @last report', ['@last' => $last]),
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $items,
      ] : NULL;
      // Write log messages to the database.
      switch ($severity) {
        case REQUIREMENT_ERROR:
          \Drupal::logger('gitinfo')->error('Status report said: <br />%value', ['%value' => $value]);
          break;

        case REQUIREMENT_WARNING:
          \Drupal::logger('gitinfo')->warning('Status report said: <br />%value', ['%value' => $value]);
          break;

        default:
          \Drupal::logger('gitinfo')->notice('Status report said OK: <br />%value', ['%value' => $value]);
          break;
      }
      $value = [
        '#markup' => $value,
      ];
      // Add elements to the render array.
      array_push($value, $errorReport, $lastReport);
    }
    // Generate the Status report info message showing Git info per directory.
    $requirements['gitinfo_requirements'] = [
      'title' => t('Git info'),
      'value' => $value,
      'severity' => $severity,
    ];
    return $requirements;
  }
}
