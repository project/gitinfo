## Formatted online version of this README
https://www.drupal.org/project/gitinfo

## Summary
An overview at _../admin/reports/gitinfo_ of fundamental Git info for all repos, from the project root to the deepest Git directory it detects. It highlights what needs attention, in that case resulting in a warning. It provides **a quick insight for projects that contain one or more Git repositories**. It is easier than searching for Git repos, then navigating to each of them in the terminal and executing [long Git commands](https://www.drupal.org/docs/8/modules/git-info-report/concept "Git Info Report - Concept - Drupal 8 guide on Drupal.org") for each piece of info. Compared with the terminal or IDE use of Git, it has the additional ability to:

*   render information as a link, for example, click on a commit hash to see the diff
*   show 'time ago' timestamps to see at a glance how long ago a commit was made
*   tell if your local branch is behind the remote fetching first automatically (optional)
*   pull automatically if there are no conflicts (optional) to keep your repos up-to-date
*   track projects outside the current one, also non-Drupal (local or on a server, optional).

## Use cases

*   **Keep track of all file changes in a development environment**, especially when working with nested Git repos.
*   **Monitor the production server for changes outside version control.**
*   **Track multiple projects** by defining extra paths outside the project root, even those in a VM or non-Drupal projects (primary site should [run natively on your OS](https://ell.stackexchange.com/questions/40139/install-some-software-natively-on-a-computer "word choice - install some software "natively" on a computer? - English Language Learners Stack Exchange")). It is suggested to have [one dedicated Drupal install to monitor multiple projects](https://www.drupal.org/docs/8/modules/git-info-report/how-to-define-directories-outside-the-project-root "How to define directories outside the project root. | Drupal 8 guide on Drupal.org") on the same server or machine.
*   **Audit** a Drupal project for Git related issues.
*   Learn more about Git by [understanding the reported items](https://www.drupal.org/docs/8/modules/git-info-report/what-gets-highlighted "What gets highlighted? | Drupal 8 guide on Drupal.org") of your own project.

## Usage

*   Find the Git Info section in the Status Report at _admin/reports/status_ with a summary.
*   Find the detailed info for all Git directories at _admin/reports/gitinfo_.
*   Settings can be configured at _admin/config/development/gitinfo_ and the containing fields are self-explanatory.

## Requirements

*   Git should be installed.
*   Unix based OS (Linux or OSX).
*   The [shell_exec()](http://php.net/manual/en/function.shell-exec.php "PHP: shell_exec - Manual") PHP function has NOT been disabled in the php.ini. If it is disabled on your server you can still use the module for your local development only. On a common PHP install, it is normally allowed. On most hosting providers as well.
*   Having at least one directory under Git version control, preferably also the project root.
*   Optionally **enable local file links** to ensure those are clickable. This is easiest accomplished with a [browser extension](https://chrome.google.com/webstore/detail/enable-local-file-links/nikfmfgobenbhmocjaaboihbeocackld "Enable local file links - Chrome Web Store"). Optionally followed by:
    *   [Force open links after downloading them. - Stack Overflow](https://stackoverflow.com/a/24290187/523688)
    *   [Set a separate directory for Chrome downloads](https://support.google.com/chrome/answer/95759?co=GENIE.Platform%3DDesktop&hl=en "Download a file - Computer - Google Chrome Help") to avoid clutter.
*   Optionally, if you want to see if **private repos** are running behind compared with their remote, you need to [assign the user www-data an SSH key to add to the remote repo](https://www.drupal.org/docs/8/modules/git-info-report/assign-the-user-www-data-an-ssh-key-to-add-to-the-remote-repo "Drupal 8 guide on Drupal.org").

## Provides per Git directory

*   [remote](https://git-scm.com/docs/git-remote "Git - git-remote Documentation") URL (linked to the repository) and **optionally a warning if the working directory is behind**, actively making a connection with the remote repo
*   [status](https://git-scm.com/docs/git-status "Git - git-status Documentation"): Current branch, untracked files and local file changes
*   the [tag](https://git-scm.com/docs/git-tag "Git - git-tag Documentation") of the last stable version
*   last [commit](https://git-scm.com/docs/git-commit "Git - git-commit Documentation") list dated (time ago) and with the commit hash that links to the commit
*   a list of commits on the remote that are not on the local branch (behind)
*   [status](https://git-scm.com/docs/git-status "Git - git-status Documentation"): The changed files and directories, neatly listed with a fuzzy timestamp (time ago)
*   [ignored](https://git-scm.com/docs/gitignore "Git - gitignore Documentation") or skipped existing directories and files
*   the [Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) it contains
*   list of [stashes](https://git-scm.com/docs/git-stash "Git - git-stash Documentation") with a fuzzy timestamp (time ago) and stash name.

On your **local development** environment, changed files indicate you have to stage (with _git add_), commit or push your work. In the report, it is all pointed out. If you find (not ignored) changed files **on your server** it usually means trouble (including hacking attempts) as they should all have been deployed under version control. The _sites/*/files_ directory and similar are in the _.gitignore_ file. It is recommended to have the complete Drupal project under Git version control to detect any changes. Use `git init` on it if this is not the case. It is usually one level above the Drupal root (the directory named _web_ or _docroot_) and contains apart from that also the project's _composer.json_ file and consequently the _vendor_ directory.

## How to define directories outside the project root

To **track a non-Drupal project** or to **monitor multiple projects** (also in VMs), read the [documentation](https://www.drupal.org/docs/8/modules/git-info-report/how-to-define-directories-outside-the-project-root "Git Info Report | Drupal 8 guide on Drupal.org") for instructions.

## Troubleshooting

See [documentation](https://www.drupal.org/docs/8/modules/git-info-report/troubleshooting "Git Info Report - Troubleshooting - Drupal 8 guide on Drupal.org").

## Setting it up only on the development environment

See [documentation](https://www.drupal.org/docs/8/modules/git-info-report/setting-it-up-only-on-development "Git Info Report | Drupal 8 guide on Drupal.org") for instructions.
