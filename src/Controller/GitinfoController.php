<?php

namespace Drupal\gitinfo\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for book routes.
 */
class GitinfoController extends ControllerBase {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Use core services object.
   */
  public function __construct(
    CacheBackendInterface $cache,
    ConfigFactoryInterface $configFactory,
    DateFormatterInterface $dateFormatter,
    LoggerChannelFactoryInterface $logger,
    Messenger $messenger,
    StateInterface $state,
    Connection $database,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->cache = $cache;
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->state = $state;
    $this->connection = $database;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default'),
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('state'),
      $container->get('database'),
      $container->get('module_handler')
    );
  }

  /**
   * Returns an overview of all Git enabled directories with detailed Git info.
   *
   * @return array
   *   A render array representing the report page content.
   */
  public function listReport() {
    $config = $this->configFactory->get('gitinfo.settings');
    $fuzzyTimestampGranularity = $config->get('fuzzy_timestamp_granularity');
    $cache = $this->cache->get('gitinfo.report');
    if (
      $cache &&
      $config->get('interval') &&
      $this->moduleHandler->moduleExists('basic_auth')
    ) {
      $value = $cache->data;
      $timestamp = $this->dateFormatter->formatInterval((time() - $cache->created), $fuzzyTimestampGranularity);
      $this->messenger->addMessage($this->t('Generated %timestamp ago. Refresh the page if needed. Refreshing toggles between the non-cached and cached version of the report.', ['%timestamp' => $timestamp]), 'warning');
      $this->messenger->addMessage($this->cache->get('gitinfo.message')->data, $this->cache->get('gitinfo.severity')->data);
      $ops = ['headfix', 'fetch', 'pull'];
      $types = ['warning', 'error', 'status'];
      foreach ($ops as $op) {
        foreach ($types as $type) {
          $message = $this->cache->get('gitinfo.' . $op . '.' . $type . '.message')->data;
          if ($message) {
            $this->messenger->addMessage($message, $this->cache->get('gitinfo.' . $op . '.' . $type . '.severity')->data);
          }
        }
      }
      $this->cache->delete('gitinfo.severity');
      $this->cache->delete('gitinfo.message');
      $this->cache->delete('gitinfo.report');
      return $value;
    }
    $timeStart = microtime(TRUE);
    $severity = 'status';
    $connect = $config->get('connect');
    $connection = FALSE;
    $gitDirs = [];
    $gitInfoModule = Link::fromTextAndUrl($this->t('Git Info Report'), Url::fromRoute('system.modules_uninstall', [], [
      'attributes' => [
        'title' => $this->t('Uninstall'),
      ],
    ]))->toString();
    // Track if an error message already kicked in to avoid being overwritten.
    $error = FALSE;
    // Generate a Status report error message if shell_exec is not available.
    if (!gitinfo_function_enabled('shell_exec')) {
      $shellExec = Link::fromTextAndUrl($this->t('shell_exec'), Url::fromUri('https://www.php.net/manual/en/function.shell-exec.php', [
        'attributes' => [
          'title' => $this->t('PHP: shell_exec - Manual'),
          'target' => '_blank',
        ],
      ]))->toString();
      $disableFunctions = Link::fromTextAndUrl($this->t('disable_functions'), Url::fromUri('https://www.php.net/manual/en/ini.core.php#ini.disable-functions', [
        'attributes' => [
          'title' => $this->t('PHP: Description of core php.ini directives - Manual'),
          'target' => '_blank',
        ],
      ]))->toString();
      $value = $this->t("Remove the PHP function @shell_exec from the @disable_functions directive in the PHP.INI settings or uninstall the @gitinfo module.", [
        '@shell_exec' => $shellExec,
        '@disable_functions' => $disableFunctions,
        '@gitinfo' => $gitInfoModule,
      ]);
      $severity = 'error';
      $error = TRUE;
    }
    // Detection method for a non-Unix OS. 'which' command is typical for Unix.
    elseif (!(strpos(shell_exec('which find'), '/find') !== FALSE)) {
      $value = $this->t("The terminal command 'find' is not available. The server's OS is probably non-Unix. Uninstall the @gitinfo module.", ['@gitinfo' => $gitInfoModule]);
      $severity = 'error';
      $error = TRUE;
    }
    else {
      // Git version info.
      $version = trim(shell_exec('git --version'));
    }
    // Generate a Status report error message if Git is not installed ..
    if (!$error && strpos($version, 'not found') !== FALSE) {
      $value = $this->t("Git not installed. Install Git or uninstall the @gitinfo module.", ['@gitinfo' => $gitInfoModule]);
      $severity = 'error';
      $error = TRUE;
    }
    if (!$error) {
      // Initialize some variables.
      $changes = 0;
      // Check the internet connection being available to check remotes.
      $response = NULL;
      // Check settings if and how we can connect to remotes (default: NULL).
      // Turn on output buffering (ob) to avoid display of command output.
      ob_start();
      passthru("ping -c 1 google.com", $response);
      ob_end_clean();
      // $response == 0 when the specified destination was reached.
      if ($response == 0) {
        $connection = $connect ? TRUE : FALSE;
      }
      // Detect the Drupal directory and extra defined directories defined in
      // the settings.php or a custom module (see README.txt).
      $extraDirectories = NULL;
      $extraDirectoriesFind = NULL;
      $extraDirectoriesArray = array_values(array_filter(explode(',', $config->get('extra_directories'))));
      // Remove the current project directory from the array if that is defined.
      $projectRoot = dirname(DRUPAL_ROOT);
      $extraDirectoriesArray = array_diff($extraDirectoriesArray, [$projectRoot]);
      $roots = array_merge([$projectRoot], $extraDirectoriesArray);
      sort($extraDirectoriesArray);
      // Put the monitored directories in an unordered list.
      $items[] = $this->t('<a href="file://@projectroot" title="View directory (enable local file links)" target="_blank">@projectroot</a> (project root)', ['@projectroot' => $projectRoot]);
      foreach ($extraDirectoriesArray as $directory) {
        if (!empty($directory)) {
          $extraDirectories .= '/' . trim($directory, '/ ') . '*/.git ';
          $extraDirectoriesFind .= '/' . trim($directory, '/ ') . ' ';
          $exists = trim(shell_exec('[ -d ' . escapeshellcmd($directory) . ' ] && echo "1"'));
          $changes = $exists ? $changes : $changes + 1;
          $linkeddirectory = $this->t('<a href="file://@value" title="View directory (enable local file links)" target="_blank">@value</a>', ['@value' => '/' . trim($directory, '/ ')]);
          $items[]['#markup'] = $exists ? $linkeddirectory : $this->t('/%directory (<span class="gitinfo-highlight">does not exist</span>, suggested is to remove it from the gitinfo.settings)', ['%directory' => trim($directory, '/ ')]);
        }
      }
      $dirsinfo = '';
      if (!empty($items)) {
        $dirsinfo = [
          '#title' => $this->t('Monitored top-level directories (roots):'),
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ];
      }
      // Get available git directories excluding those in vendor directories.
      $gitDirs = shell_exec('cd ' . $projectRoot . ' && find $PWD ' . escapeshellcmd($extraDirectoriesFind) . ' -name "*.git" -type d | grep -e ".git$" | grep -v "/vendor/.*" | sort');
      // Remove '/.git' to have the clean repo directory path.
      $gitDirs = str_replace('/.git', '', $gitDirs);
      // Store all available git directories into an array.
      $gitDirs = array_unique(array_filter(explode(PHP_EOL, $gitDirs)));
      foreach ($roots as $root) {
        if (!in_array($root, $gitDirs)) {
          array_push($gitDirs, $root);
        }
      }
      // Get available git directories but only in vendor directories.
      $vendorGitDirs = shell_exec('cd ' . $projectRoot . ' && find $PWD ' . escapeshellcmd($extraDirectoriesFind) . ' -name "*.git" -type d | grep -e "/vendor/.*.git$" | sort');
      // Remove '/.git' to have the clean repo directory path.
      $vendorGitDirs = str_replace('/.git', '', $vendorGitDirs);
      // Store all available git directories into an array.
      $vendorGitDirs = array_filter(explode(PHP_EOL, $vendorGitDirs));
      foreach ($vendorGitDirs as $vendorGitDir) {
        $vendorGitDir = $this->t('<a href="file://@file" title="View vendor directory (enable local file links)" target="_blank">@file</a>', ['@file' => $vendorGitDir]);
        $vendorGitDirlinks[] = $vendorGitDir;
      }
      $vendorDirsInfo = '';
      if (!empty($vendorGitDirs)) {
        $vendorDirsInfo = [
          '#title' => $this->t('Composer vendor directories using git clone (source) instead of a package (dist)'),
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $vendorGitDirlinks,
        ];
      }

      // Generate a warning message if no Git directories were found.
      if (empty($gitDirs)) {
        $gitInfo = [
          '#markup' => '<p>' . $this->t("<span class='gitinfo-highlight'>No git directories can be detected.</span> Check the directory permissions.") . '</p>',
        ];
        $value = [$dirsinfo, $gitInfo, $vendorDirsInfo];
        $value['#attached']['library'][] = 'gitinfo/gitinfo';
        $severity = 'warning';
      }
      else {
        asort($gitDirs);
        $prevDir = NULL;
        $responses = [];
        // Only get reused configuration or values once.
        $numberCommitMessages = $config->get('number_commit_messages');
        $numberCommitMessages = $numberCommitMessages < 0 ? NULL : ' -' . $numberCommitMessages;
        $exactDateFormat = $config->get('exact_date_format');
        $whichGit = trim(shell_exec('which git'));
        // User info.
        $user = trim(shell_exec('whoami'));
        $otherUser = trim(strstr(shell_exec('who -q'), PHP_EOL, TRUE));
        // Only note another user if different.
        $otherUser = $user != $otherUser ? $this->t('Not as <strong>%otheruser</strong>.', ['%otheruser' => $otherUser]) : NULL;
        // Loop through all git directories to output the info per directory.
        foreach ($gitDirs as $value) {
          // Reset any remaining values from the previous cycle.
          $remote = NULL;
          $commitsBehind = NULL;
          $status = NULL;
          $modList = NULL;
          $tag = NULL;
          $lastCommits = NULL;
          $ignored = NULL;
          $ignoredCmdLinks = NULL;
          $submodules = NULL;
          $stash = NULL;
          // Get current status.
          $statusCmd = trim(shell_exec('cd ' . escapeshellarg($value) . ' && git status 2>&1'));
          $branch = trim(shell_exec('cd ' . escapeshellarg($value) . ' && git rev-parse --abbrev-ref HEAD'));
          $project = basename($value);
          // Fix HEAD detached state.
          if (
            $config->get('head_detached') &&
            strpos($statusCmd, 'HEAD detached') !== FALSE &&
            strpos($statusCmd, 'nothing to commit, working tree clean') !== FALSE
          ) {
            $checkout = trim(shell_exec('cd ' . escapeshellarg($value) . ' && ' . escapeshellcmd($whichGit) . ' checkout @{-1}'));
            $refLog = $checkout ? trim(shell_exec('cd ' . escapeshellarg($value) . ' && git reflog -1 2>&1')) : $this->t('No reposnse: Checkout of the last known branch FAILED.');
            if ($checkout) {
              $messageType = 'status';
            }
            else {
              $messageType = 'error';
            }
            // Numbered index is used in case of duplicate project names.
            $responses['headfix'][$messageType][$project][] = [
              'directory' => $value,
              'response' => $refLog,
            ];
          }
          // Link to local index. To have the link working you need:
          // https://github.com/tksugimoto/chrome-extension_open-local-file-link
          $dir = $value == $projectRoot || in_array($value, $extraDirectoriesArray) ? $this->t('<h2><a href="file://@value" title="View directory (enable local file links)" target="_blank">@value</a> (root)</h2>', ['@value' => $value]) : $this->t('<a href="file://@value" title="View directory (enable local file links)" target="_blank">@value</a>', ['@value' => $value]);
          // Make shell command safe.
          // See https://stackoverflow.com/a/22739494/523688.
          $remoteCmd = trim(shell_exec('cd ' . escapeshellarg($value) . ' && git config --get remote.origin.url'));
          $remoteUrl = strpos($remoteCmd, 'http') === 0 ? $remoteCmd : preg_replace('/:/', '/', $remoteCmd);
          $remoteUrl = preg_replace([
            '/^git@/',
            '/\.git$/',
          ], [
            'https://',
            '',
          ], $remoteUrl);
          $parse = parse_url($remoteUrl);
          $host = isset($parse['host']) ? $parse['host'] : NULL;
          switch ($host) {
            case 'git.drupal.org':
            case 'git.drupalcode.org':
            case 'gitlab.com':
            case 'github.com':
              $tree = '/tree/' . $branch;
              break;

            case 'bitbucket.com':
              $tree = '/src/' . $branch . '/';
              break;

            default:
              $tree = NULL;
              break;
          }
          $needsPull = $connection || !$connect ? $this->t('Connecting to the remote to check the status is disabled in the settings.') : $this->t('There is currently no internet connection available to check the remote.');
          if ($connect && $remoteCmd) {
            // Fetch and see if successfull. Output contains project name.
            $fetch = $connection ? shell_exec("cd " . escapeshellarg($value) . ' && ' . escapeshellcmd($whichGit) . ' fetch -v 2>&1') : NULL;
            shell_exec("cd " . escapeshellarg($value) . ' && chmod 666 .git/FETCH_HEAD');
            if (stripos($fetch, 'FETCH_HEAD: Permission denied') !== FALSE) {
              $messageType = 'error';
            }
            elseif (!(stripos($fetch, $project) !== FALSE)) {
              $messageType = 'warning';
            }
            else {
              $messageType = 'status';
            }
            // Numbered index is used in case of duplicate project names.
            $responses['fetch'][$messageType][$project][] = [
              'directory' => $value,
              'response' => $fetch,
            ];
            // Get the remote commits that are not on local.
            $commitsBehind = $connection ? array_filter(explode(PHP_EOL, shell_exec("cd " . escapeshellarg($value) . ' && git log HEAD..@{u} --format="%at⁏%an⁏%ae⁏%cn⁏%ce⁏%h⁏%s"'))) : NULL;
            // Ignore false positives.
            $commitsBehind = (
              strpos($statusCmd, 'Your branch is up to date with') !== FALSE ||
              strpos($statusCmd, 'HEAD detached') !== FALSE
            ) ? NULL : $commitsBehind;
            $lastCommits = $this->commitList($commitsBehind, $exactDateFormat, $fuzzyTimestampGranularity, $remoteUrl);
            $commitsBehind = $this->collapseLongList($lastCommits, $this->formatPlural(count($lastCommits), 'Local branch is 1 commit behind remote', 'Local branch is @count commits behind remote'), FALSE);
            // Remote is accessible.
            if ($connection) {
              $needsPull = empty($commitsBehind) ? NULL : $this->t('Pull needed.');
              $changes = empty($commitsBehind) ? NULL : $changes + 1;
            }
            // A switch is used in case other options are added in the future.
            switch ($connect) {
              case 'pull':
                if (!(
                  strpos($statusCmd, 'Your branch is up to date with') !== FALSE ||
                  strpos($statusCmd, 'HEAD detached') !== FALSE ||
                  strpos($statusCmd, 'No commits yet') !== FALSE ||
                  empty($remoteCmd)
                )) {
                  $merge = trim(shell_exec("cd " . escapeshellarg($value) . ' && ' . escapeshellcmd($whichGit) . ' config user.email "temporary@email.com" && ' . escapeshellcmd($whichGit) . ' merge FETCH_HEAD --ff-only 2>&1'));
                  shell_exec("cd " . escapeshellarg($value) . ' && ' . escapeshellcmd($whichGit) . ' config --unset user.email');
                  if ($merge) {
                    $messageType = strpos($merge, 'Updating') !== FALSE ? 'status' : 'error';
                    $messageType = strpos($merge, 'error:') !== FALSE ? 'warning' : $messageType;
                  }
                  else {
                    $messageType = 'error';
                    $merge = $this->t('No response: Merge FAILED.');
                  }
                  // Numbered index is used in case of duplicate project names.
                  $responses[$connect][$messageType][$project][] = [
                    'directory' => $value,
                    'response' => $merge,
                  ];
                }
                break;
            }
          }
          if (strpos($remoteUrl, 'http') === 0) {
            $remotelink = Link::fromTextAndUrl($remoteCmd, Url::fromUri($remoteUrl . $tree, [
              'attributes' => [
                'title' => $this->t('View repository'),
                'target' => '_blank',
              ],
            ]))->toString();
          }
          else {
            $remotelink = $remoteCmd;
          }
          if (!empty($remoteCmd)) {
            $remote = $this->t('Remote: @remote (<strong>@branch</strong>). @needspull', [
              '@remote' => $remotelink,
              '@branch' => $branch,
              '@needspull' => $needsPull,
            ]);
          }
          elseif (strpos($statusCmd, 'not a git repository') !== FALSE) {
            $remote = $this->t("Not a git repository. Suggested is to run 'git init' on a project root.");
          }
          else {
            $remote = $this->t('Remote: Not added yet');
          }
          // Refresh 'git'status' if changed due to checkout, fetch or pull.
          $statusCmd = trim(shell_exec('cd ' . escapeshellarg($value) . ' && git status'));
          // Limit if too long.
          $statusCmd = !empty($statusCmd) && strlen($statusCmd) > 600 ? substr($statusCmd, 0, 600) . ' ..' : $statusCmd;
          // Add a closing period if not empty.
          $status = !empty($statusCmd) ? $statusCmd . '.' : NULL;
          // Get list of modified files incl. untracked, deleted, renamed, etc.
          $modList = shell_exec('cd ' . escapeshellarg($value) . ' && git status -s | while read mode file; do echo $mode $(stat -c %Y $file) $file; done');
          $modListLines = array_filter(explode(PHP_EOL, $modList));
          $modList = NULL;
          // Replace a file's change status code with a human readable string.
          $target = [
            '/^M$/',
            '/^A$/',
            '/^D$/',
            '/^R$/',
            '/^C$/',
            '/^U$/',
            '/^\?\?$/',
            '/^M(?=[MADRCU?])/',
            '/^A(?=[MADRCU?])/',
            '/^D(?=[MADRCU?])/',
            '/^R(?=[MADRCU?])/',
            '/^C(?=[MADRCU?])/',
            '/^U(?=[MADRCU?])/',
            '/(?=\w)M$/',
            '/(?=\w)A$/',
            '/(?=\w)D$/',
            '/(?=\w)R$/',
            '/(?=\w)C$/',
            '/(?=\w)U$/',
            '/\w+ \?/',
            '/^[^MADRCU?]{1,2}/',
          ];
          $replacement = [
            'Modified',
            'Added',
            'Deleted',
            'Renamed',
            'Copied',
            'Updated but unmerged',
            'Untracked',
            'Modified and ',
            'Added and ',
            'Deleted and ',
            'Renamed and ',
            'Copied and ',
            'Updated but unmerged and ',
            'modified',
            'added',
            'deleted',
            'renamed',
            'copied',
            'updated but unmerged',
            'with untracked content',
            'Unknown change',
          ];
          foreach ($modListLines as $modListline) {
            $modListLineParts = explode(' ', $modListline);
            $modListLineParts[0] = preg_replace($target, $replacement, $modListLineParts[0]);
            if ((int) $modListLineParts[1]) {
              $modListDateFuzzy = $this->dateFormatter->formatInterval((time() - $modListLineParts[1]), $fuzzyTimestampGranularity);
              $modListDateHover = date($exactDateFormat, $modListLineParts[1]);
              $modListLineParts[1] = $this->t('<a title="@modlistdatehover">@modlistdatefuzzy</a> ago', [
                '@modlistdatehover' => $modListDateHover,
                '@modlistdatefuzzy' => $modListDateFuzzy,
              ]);
            }
            else {
              $modListLineParts[1] = $this->t('<em>@file</em>', ['@file' => $modListLineParts[1]]);
            }
            $modListLineParts[2] = isset($modListLineParts[2]) ? $this->t('<a href="file://@value/@file" title="View file (enable local file links)" target="_blank">@file</a>', [
              '@value' => $value,
              '@file' => $modListLineParts[2],
            ]) : NULL;
            $modListLineGlue = implode(' ', $modListLineParts);
            $modList[] = $this->t($modListLineGlue);
          }
          $modListList = $this->collapseLongList($modList, $this->t("<span class='gitinfo-highlight'>Changed files and directories</span>"));
          // Get all tags on the current HEAD.
          $tagCmd = trim(shell_exec('cd ' . escapeshellarg($value) . ' && git tag --points-at HEAD'));
          // Add a label if not empty.
          $tag = !empty($tagCmd) ? $this->t('Tag: @tag (latest stable)', ['@tag' => $tagCmd]) : NULL;
          // Use one git command to get multiple commit data by exploding.
          // The separator is a rarely used character '⁏' (reversed semicolon).
          $commits = array_filter(explode(PHP_EOL, shell_exec('cd ' . escapeshellarg($value) . ' && git log' . escapeshellcmd($numberCommitMessages) . ' --format="%at⁏%an⁏%ae⁏%cn⁏%ce⁏%h⁏%s"')));
          // Get the local commits that are not on remote (hash only).
          $commitsAhead = $connection ? array_filter(explode(PHP_EOL, shell_exec("cd " . escapeshellarg($value) . ' && git log @{u}..HEAD --format="%h" 2>&1'))) : NULL;
          $lastCommits = $this->commitList($commits, $exactDateFormat, $fuzzyTimestampGranularity, $remoteUrl, $commitsAhead);
          // Do not start the sentence with 'Last' if all commits are shown.
          $startWord = !$numberCommitMessages || count($commits) < abs((int) $numberCommitMessages) ? $this->t('Total of') : $this->t('Last');
          $lastCommits = $this->collapseLongList($lastCommits, $this->formatPlural(count($lastCommits), '@startword 1 commit on local', '@startword @count commits on local', ['@startword' => $startWord]), FALSE);
          $ignoredCmd = shell_exec('cd ' . escapeshellarg($value) . ' && git clean -ndX');
          // Turn the string into an array based on linebreaks removing empties.
          $ignoredCmd = array_filter(explode(PHP_EOL, str_replace([
            'Would remove ',
            'Would skip repository ',
          ], [
            $this->t('Ignoring') . ' ',
            $this->t('Skipping repository') . ' ',
          ], $ignoredCmd)));
          // Cluster the items of the defined directories.
          $aggregates = $config->get('aggregate_directories');
          $aggregates = explode(PHP_EOL, $aggregates);
          foreach ($aggregates as $aggregate) {
            $aggregateCount = count($ignoredCmd);
            $ignoredCmd = array_filter($ignoredCmd, function ($v) use ($aggregate) {
              return !preg_match('/^Ignoring ' . str_replace('/', '\/', $aggregate) . '\//', $v);
            });
            $aggregateCount = $aggregateCount - count($ignoredCmd);
            if ($aggregateCount) {
              $total = trim(shell_exec('cd ' . escapeshellarg($value) . '/' . escapeshellarg($aggregate) . ' && ls | wc -l'));
              $aggregateCount = $aggregateCount > $total ? $total : $aggregateCount;
              $ignoredCmd[] = $this->formatPlural($aggregateCount, "Ignoring one item in <a href=\"file://@value/@aggregate\" title=\"View directory (enable local file links)\" target=\"_blank\">@aggregate/</a>", "Ignoring @count of @total items in <a href=\"file://@value/@aggregate\" title=\"View directory (enable local file links)\" target=\"_blank\">@aggregate/</a>", [
                '@value' => $value,
                '@aggregate' => $aggregate,
                '@total' => $total,
              ]);
            }
          }
          foreach ($ignoredCmd as $item) {
            if (is_string($item)) {
              preg_match('/[^\s]+$/', $item, $matches);
              $ignoredCmdLinks[] = $this->t(preg_replace('/[^\s]+$/', '<a href="file://@value/@match" title="View (enable local file links)" target="_blank">@match</a>', $item), ['@value' => $value, '@match' => $matches[0]]);
            }
            else {
              $ignoredCmdLinks[] = $item;
            }
          }
          // Add the items to the render array.
          $ignored = $this->collapseLongList($ignoredCmdLinks, $this->t('Ignored or skipped <strong>existing</strong> directories or files. Ignored as for the <em>.gitignore</em>. Skipped because a Git repo itself'));
          $submodulesCmd = shell_exec('cd ' . escapeshellarg($value) . ' && git submodule status | cut -d\' \' -f3-4');
          // Turn the string into an array based on linebreaks removing empties.
          $submodulesCmd = array_filter(explode(PHP_EOL, $submodulesCmd));
          $submodules = $this->collapseLongList($submodulesCmd, $this->t('Submodules'));
          $stashCmd = shell_exec('cd ' . escapeshellarg($value) . ' && git stash list --format=\'%gd (%cr): %gs\'');
          // Turn the string into an array based on linebreaks removing empties.
          $stashCmd = array_filter(explode(PHP_EOL, $stashCmd));
          if (empty($stashCmd)) {
            $stash = NULL;
          }
          else {
            $stash = $this->collapseLongList($stashCmd, $this->t("<span class='gitinfo-highlight'>Stash list</span>, stays local only but should be temporary. Consider to 'pop', 'drop' or 'clear'."));
            $changes++;
          }
          // Group Git directories within the same directory by a ruler.
          $dirParts = explode('/', $value);
          array_pop($dirParts);
          $ruler = $dirParts === $prevDir && !in_array(implode('/', $dirParts), $extraDirectoriesArray) ? NULL : new FormattableMarkup('<hr /><hr />', []);
          $prevDir = $dirParts;
          // Generate the list of Git info for currently processed directory.
          $output['items'][] = [
            '#markup' => $this->t('@ruler<h5>@dir</h5>', ['@ruler' => $ruler, '@dir' => $dir]),
            'children' => array_filter([
              $remote,
              $status,
              $tag,
              $commitsBehind,
              $lastCommits,
              $modListList,
              $ignored,
              $submodules,
              $stash,
            ]),
          ];
        }
        $gitInfo = '';
        // Generate an unordered list.
        if (!empty($output)) {
          $gitInfo = [
            '#title' => $this->t("Detected Git directories (recursive, excluding Composer 'vendor' directories)"),
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#items' => $output['items'],
          ];
          // Highlight any changes to bring it to attention.
          $highlight = [
            'Changed but not updated',
            'Changes not staged for commit',
            'Changes to be committed',
            'Your branch is ahead',
            'Your branch is behind',
            'HEAD detached',
            'No commits yet',
            'Remote: Not added yet',
            'Untracked files',
            'files would be overwritten by merge',
            "A 'git pull' failed",
            "Pull needed",
            "behind remote",
            "ahead remote",
            "Not a git repository",
          ];
          foreach ($gitInfo['#items'] as $key => $item) {
            foreach ($gitInfo['#items'][$key]['children'] as $index => $value) {
              $gitInfo['#items'][$key]['children'][$index] = is_array($gitInfo['#items'][$key]['children'][$index]) ? $gitInfo['#items'][$key]['children'][$index] : $this->t(preg_replace("/\w*?(?:" . implode('|', $highlight) . ")\w*/i", "<span class='gitinfo-highlight'>$0</span>", $gitInfo['#items'][$key]['children'][$index], -1, $count));
              $changes = $changes + $count;
            }
          }
        }
        $value = [$dirsinfo, $gitInfo, $vendorDirsInfo];
        // If we have changes.
        if ($changes) {
          // Make it a warning.
          $severity = 'warning';
        }
        // Log status, warning and error responses from Git operations.
        if ($responses) {
          foreach ($responses as $operation => $response) {
            foreach ($response as $messageType => $projects) {
              $items = [];
              foreach ($projects as $projectName => $results) {
                foreach ($results as $result) {
                  $items[] = $this->t('Project <strong>%project</strong> in %directory gives the following response:<br />%response', [
                    '%project' => $projectName,
                    '%directory' => $result['directory'],
                    '%response' => $result['response'],
                  ]);
                }
              }
              $instructions = Link::fromTextAndUrl($this->t('Give the user <strong>%user</strong> access to the <strong>private</strong> repositories', ['%user' => $user]), Url::fromUri('https://www.drupal.org/node/3089443', [
                'attributes' => [
                  'title' => $this->t('Git Info Report - Assign the user www-data an SSH key and add it to the remote repo - Drupal 8 guide on Drupal.org'),
                  'target' => '_blank',
                ],
              ]))->toString();
              $privateReposNote = $connection ? $this->t("A <em>git fetch</em> is executed as user <strong>%user</strong>. @otheruser @instructions to allow fetching also on those.", [
                '%user' => $user,
                '@otheruser' => $otherUser,
                '@instructions' => $instructions,
              ]) : $needsPull;
              $allowGit = Link::fromTextAndUrl($this->t("allow the user <strong>%user</strong> to perform <em>git fetch</em>", ['%user' => $user]), Url::fromUri('https://www.drupal.org/node/3142770', [
                'attributes' => [
                  'title' => $this->t('Drupal guide on Drupal.org'),
                  'target' => '_blank',
                ],
              ]))->toString();
              $action = NULL;
              if ($messageType === 'error') {
                $action = $operation === 'fetch' || $operation === 'headfix' ? $this->t("A simple file permission change is required to $allowGit.", [
                  '@allow_git' => $allowGit ,
                ]) : $this->t("Check the messages for FETCH to see the suggested action to take or try a manual 'git pull' without the '--ff-only' flag that is used by the automated method to prevent creation of a new SHA (hash).");
              }
              elseif ($messageType === 'warning' && $operation === 'fetch') {
                $action = $privateReposNote;
              }
              switch ($operation) {
                case 'pull':
                  $operationMessage = $this->t('GIT PULL');
                  switch ($messageType) {
                    case 'error':
                      $typeMessage = $this->t('FAILED');
                      break;

                    case 'warning':
                      $typeMessage = $this->t('ABORTED, most likely because of <strong>merge conflicts</strong>,');
                      break;

                    default:
                      $typeMessage = $this->t("OK (REMINDER: run <em>'composer install'</em>, <em>'drush updb'</em> and <em>'drush cim'</em>)");
                      break;
                  }
                  break;

                case 'headfix':
                  $operationMessage = $this->t("GIT 'HEAD detached' state");
                  switch ($messageType) {
                    case 'error':
                      $typeMessage = $this->t("FAILED being fixed, likely because of <strong>file permissions</strong>,");
                      break;

                    default:
                      $typeMessage = $this->t('has been fixed OK (successfully returned to the last known branch)');
                      break;
                  }
                  break;

                case 'fetch':
                  $operationMessage = $this->t("GIT FETCH");
                  switch ($messageType) {
                    case 'error':
                      $typeMessage = $this->t("FAILED, likely because of <strong>file permissions</strong>,");
                      break;

                    case 'warning':
                      $typeMessage = $this->t("FAILED, likely because of denied <strong>repository access</strong>,");
                      break;

                    default:
                      $typeMessage = $this->t('OK, showing if local is ahead or behind the remote repo,');
                      break;
                  }
                  break;

                default:
                  // code...
                  break;
              }
              // Count occurences to store as report metadata.
              $count = isset($responses[$operation][$messageType]) ? count($responses[$operation][$messageType]) : 0;
              $resultsList = [
                '#theme' => 'item_list',
                '#list_type' => 'ul',
                '#items' => $items,
              ];
              $resultsList = render($resultsList);
              $logMessage = $this->t("@action<br />@operationMessage @typeMessage on the following projects:<br />@resultsList", [
                '@action' => $action,
                '@operationMessage' => $operationMessage,
                '@typeMessage' => $typeMessage,
                '@resultsList' => $resultsList,
              ]);
              $logType = $messageType == 'status' ? 'notice' : $messageType;
              $this->logger->get('gitinfo')->$logType($logMessage);
              $logUrl = $this->getLogUrlFromType('gitinfo');
              $screenMessage = $this->formatPlural($count, '@operationMessage @typeMessage on <a href="@logUrl">1 project</a>. @action', '@operationMessage @typeMessage on <a href="@logUrl">@count projects</a>. @action', [
                '@operationMessage' => $operationMessage,
                '@typeMessage' => $typeMessage,
                '@logUrl' => $logUrl,
                '@action' => $action,
              ]);
              $this->messenger->addMessage($screenMessage, $messageType);
              $this->cache->set('gitinfo.' . $operation . '.' . $messageType . '.severity', $messageType, CacheBackendInterface::CACHE_PERMANENT);
              $this->cache->set('gitinfo.' . $operation . '.' . $messageType . '.message', $screenMessage, CacheBackendInterface::CACHE_PERMANENT);
            }
          }
        }
      }
    }
    // Render a status message.
    $infoLink = Link::fromTextAndUrl($this->t("Explanation and what to do"), Url::fromUri('https://www.drupal.org/node/3088107', [
      'attributes' => [
        'title' => $this->t('Git Info Report - Drupal 8 guide on Drupal.org'),
        'target' => '_blank',
      ],
    ]))->toString();
    $timeEnd = microtime(TRUE);
    $executionTime = ceil($timeEnd - $timeStart);
    // Store state info to render in the Status report.
    $state = [
      'gitinfo.generated' => time(),
      'gitinfo.severity' => $severity,
      'gitinfo.etime' => $executionTime,
      'gitinfo.connect' => $connect,
      'gitinfo.connection' => $connection,
      'gitinfo.monitored' => count($gitDirs),
    ];
    if ($severity !== 'error') {
      $this->state->setMultiple($state);
    }
    // Add labels and format.
    $items = isset($state['gitinfo.generated']) ? [
      'generated' => $this->t('Generated for %monitored directories in %etime', [
        '%monitored' => $state['gitinfo.monitored'],
        '%etime' => $this->dateFormatter->formatInterval($state['gitinfo.etime'], 2),
      ]),
      'severity' => $this->t('Severity: %severity', ['%severity' => $state['gitinfo.severity']]),
      'connect' => $this->t('Remote connection %connection. Configured to %connect.', [
        '%connect' => empty($state['gitinfo.connect']) ? 'not connect' : $state['gitinfo.connect'],
        '%connection' => $state['gitinfo.connection'] ? $this->t('was successfully established') : $this->t('was not established'),
      ]),
    ] : NULL;
    $items = array_filter($items);
    $metadata = $items ? [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $items,
    ] : NULL;
    $metadata = render($metadata);
    // Log the report results.
    switch ($severity) {
      case 'error':
        $message = $this->t("Git info could not be generated. @value", ['@value' => $value]);
        $this->logger->get('gitinfo')->error($message);
        break;

      case 'warning':
        $message = $this->t("Git related issues were found. Check the highlighted issues in the report. @infolink.@metadata", ['@infolink' => $infoLink, '@metadata' => $metadata]);
        $this->logger->get('gitinfo')->warning($message);
        break;

      default:
        $message = $this->t("OK: No Git related issues were found.@metadata", ['@metadata' => $metadata]);
        $this->logger->get('gitinfo')->notice($message);
        break;
    }
    // Notify the user of the report results.
    $this->messenger->addMessage($message, $severity);
    // Module links Help and Configure.
    $helpLink = Link::fromTextAndUrl($this->t("Help"), Url::fromUri('internal:/admin/help/gitinfo', [
      'attributes' => [
        'class' => ['module-link', 'module-link-help'],
      ],
    ]))->toString();
    $configureLink = Link::fromTextAndUrl($this->t("Configure"), Url::fromRoute('gitinfo.settings', [], [
      'attributes' => [
        'class' => ['module-link', 'module-link-configure'],
      ],
    ]))->toString();
    $links = [
      '#markup' => '<div class="gitinfo links">' . $helpLink . $configureLink . '</div>',
    ];
    // Add the links to the render array.
    array_unshift($value, $links);
    // Add CSS.
    $value['#attached']['library'][] = 'gitinfo/gitinfo';
    $this->cache->set('gitinfo.severity', $severity, CacheBackendInterface::CACHE_PERMANENT);
    $this->cache->set('gitinfo.message', $message, CacheBackendInterface::CACHE_PERMANENT);
    $this->cache->set('gitinfo.report', $value, CacheBackendInterface::CACHE_PERMANENT);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function commitList($commits, $exactDateFormat, $fuzzyTimestampGranularity, $remoteUrl, $ahead = NULL) {
    $lastCommits = [];
    if (is_array($commits) || is_object($commits)) {
      foreach ($commits as $commit) {
        $commit = explode('⁏', $commit);
        if ($commit[0]) {
          $lastCommitDateHover = date($exactDateFormat, $commit[0]);
          $lastCommitDateFuzzy = $this->dateFormatter->formatInterval((time() - $commit[0]), $fuzzyTimestampGranularity);
          // See if the author and the committer are the same.
          $people = $commit[1] == $commit[3] ? $this->t('by @committerauthor (@committerauthormail)', [
            '@committerauthor' => $commit[1],
            '@committerauthormail' => $commit[2],
          ]) : $this->t('authored by @author (@authormail) and committed by @committer (@committermail)', [
            '@author' => $commit[1],
            '@authormail' => $commit[2],
            '@committer' => $commit[3],
            '@committermail' => $commit[4],
          ]);
          $isAhead = empty($ahead) ? FALSE : in_array($commit[5], $ahead);
          if (strpos($remoteUrl, 'http') === 0 && !$isAhead) {
            $hash = Link::fromTextAndUrl($commit[5], Url::fromUri($remoteUrl . '/commit/' . $commit[5], [
              'attributes' => [
                'title' => $this->t('View commit'),
                'target' => '_blank',
              ],
            ]))->toString();
          }
          elseif ($isAhead) {
            $hash = $commit[5] . ' ' . $this->t("(AHEAD: 'git push' to the remote)");
          }
          else {
            $hash = $commit[5];
          }
          // Add period at end of string if there is not one there already.
          $message = !is_null($commit[6]) && substr(trim($commit[6]), -1) != '.' ? trim($commit[6]) . '.' : trim($commit[6]);
          // Only include the last commit if it exists.
          $lastcommit = $this->t('<a title="@lastcommit_datehover">@lastcommit_datefuzzy</a> ago @people with hash @hash:<br />@message', [
            '@lastcommit_datehover' => $lastCommitDateHover,
            '@lastcommit_datefuzzy' => $lastCommitDateFuzzy,
            '@people' => $people,
            '@hash' => $hash,
            '@message' => $message,
          ]);
          $lastCommits[] = $lastcommit;
        }
      }
    }
    return $lastCommits;
  }

  /**
   * {@inheritdoc}
   */
  public function collapseLongList($list, $title, $showCount = TRUE) {
    if (is_array($list)) {
      $config = $this->configFactory->get('gitinfo.settings');
      if (count($list) < $config->get('threshold_list') && !$config->get('list_always_collabsible')) {
        $thresholdList = empty($list) ? NULL : [
          '#markup' => $title,
          'children' => $list,
        ];
      }
      else {
        $state = count($list) < $config->get('threshold_list') ? TRUE : FALSE;
        $count = $showCount ? $this->formatPlural(count($list), '(1 item)', '(@count items)') : NULL;
        $thresholdList = empty($list) ? NULL : [
          '#type' => 'details',
          '#title' => $this->t("@title @count", ['@title' => $title, '@count' => $count]),
          '#open' => $state,
          'children' => $list,
        ];
      }

      return $thresholdList;
    }
    else {
      return NULL;
    }
  }

  /**
   * Returns a link to the last logged event based on the type.
   *
   * @param string $type
   *   (optional) The message type, usually the module that generates the log
   *   entry. If left empty it shows the last PHP message.
   * @param bool $oldestLog
   *   (optional) If TRUE the oldest log occurence found in the log will be
   *   returned, otherwise the newest log that match with the criterias will be
   *   returned.
   *
   * @return string|bool
   *   A link to the last event log generated. FALSE if no message was found.
   */
  public function getLogUrlFromType($type = 'php', $oldestLog = FALSE) {
    $query = $this->connection->select('watchdog', 'w');

    $query->fields('w', ['wid'])->condition('type', $type);

    if (empty($oldestLog)) {
      $query->orderBy('wid', 'DESC');
    }

    $eventId = $query->range(0, 1)->execute()->fetchField();

    if (!empty($eventId)) {
      return Url::fromRoute('dblog.event', ['event_id' => $eventId])->toString();
    }
    return FALSE;
  }

}
