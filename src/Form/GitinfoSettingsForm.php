<?php

namespace Drupal\gitinfo\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GitinfoSettingsForm.
 */
class GitinfoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gitinfo.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gitinfo_settings';
  }

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Use core services object.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    DateFormatterInterface $dateFormatter,
    Messenger $messenger,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
    $this->messenger = $messenger;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('messenger'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('gitinfo.settings');
    if ($config->hasOverrides()) {
      $this->messenger->addMessage($this->t("<em>'Extra directories to monitor outside the current project root'</em> are defined in the <em>settings.php</em> file using <a href='https://www.drupal.org/docs/8/api/configuration-api/configuration-override-system'>Drupal's configuration override system</a>. The corresponding field is locked and cannot be modified."), 'warning');
    }
    $gitInfoReport = Link::fromTextAndUrl($this->t('Git Info Report'), Url::fromRoute('gitinfo.report'))->toString();
    $form['link'] = [
      '#type' => 'item',
      '#description' => $this->t("Configuration for the @gitinfo_report.", ['@gitinfo_report' => $gitInfoReport]),
    ];
    $cronDisabled = FALSE;
    if (!$this->moduleHandler->moduleExists('basic_auth')) {
      $cronDisabled = TRUE;
      $this->messenger->addMessage($this->t("The <em>cron</em> settings are disabled until the needed core module <em>'HTTP Basic Authentication'</em> is enabled. The corresponding fields are locked and cannot be modified."), 'warning');
    }
    $form['cron'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron'),
      '#disabled' => $cronDisabled,
    ];
    $form['cron']['interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Interval'),
      '#description' => $this->t('Generates a report to monitor a server or to load it faster from cache (can be refreshed).'),
      '#default_value' => $config->get('interval'),
      '#options' => [
        0 => $this->t('never'),
        60 => $this->t('1 minute - use only for testing'),
        600 => $this->t('10 minutes'),
        3600 => $this->t('1 hour'),
        86400 => $this->t('1 day'),
        604800 => $this->t('1 week'),
      ],
    ];
    $form['cron']['cron_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name of a dedicated cron user'),
      '#description' => $this->t("It is strongly advised to create a dedicated role 'Cron' with only the permission to 'View site reports'."),
      '#default_value' => $config->get('cron_user'),
      '#size' => 30,
      '#maxlength' => 60,
    ];
    $form['cron']['cron_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Password of a dedicated cron user'),
      '#description' => $this->t("Must be identical to the corresponding user password."),
      '#attributes' => ['value' => $config->get('cron_pass')],
      '#size' => 30,
      '#maxlength' => 60,
    ];
    $allowGit = Link::fromTextAndUrl($this->t("allow the user <strong>%user</strong> to <em>fetch</em> and <em>checkout</em>", ['%user' => trim(shell_exec('whoami'))]), Url::fromUri('https://www.drupal.org/node/3142770', [
      'attributes' => [
        'title' => $this->t('Drupal guide on Drupal.org'),
        'target' => '_blank',
      ],
    ]))->toString();
    $form['assistance'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Git assistance'),
      '#description' => $this->t("<strong>NOTE</strong>: When selecting any Git assistance option a simple file permission change is required to $allowGit.", ['@allow_git' => $allowGit]),
    ];
    $form['assistance']['connect'] = [
      '#type' => 'radios',
      '#title' => $this->t('Connection with the remote repos from least to most intrusive but all safe'),
      '#description' => $this->t("<strong>Connecting slows down report generation</strong>."),
      '#options' => [
        '' => $this->t('Do not connect - no assistance'),
        'fetch' => $this->t("Do a <em>git fetch</em> to show all commits on the remote that are not on the local branch"),
        'pull' => $this->t("Attempt a <em>git pull</em> but only if needed (local is behind remote) and not conflicting (only intended for a local development environment, in any case, local changes will always be preserved)"),
      ],
      '#default_value' => $config->get('connect'),
    ];
    $form['assistance']['head_detached'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("<strong>Fix a 'HEAD detached' state</strong>"),
      '#description' => $this->t("Does a checkout of the previous known branch if no changes are detected. Safe to use."),
      '#default_value' => $config->get('head_detached'),
    ];
    $useGlobal = Link::fromTextAndUrl($this->t('define extra directories in the <em>settings.php instead</em>'), Url::fromUri('https://www.drupal.org/node/3086620#s-define-them-in-the-settingsphp', [
      'attributes' => [
        'title' => $this->t('Git Info Report - How to define directories outside the project root | Drupal 8 guide on Drupal.org'),
        'target' => '_blank',
      ],
    ]))->toString();
    $instructions = Link::fromTextAndUrl($this->t('give the user <strong>%user</strong> access to the <strong>private</strong> repositories', ['%user' => trim(shell_exec('whoami'))]), Url::fromUri('https://www.drupal.org/node/3089443', [
      'attributes' => [
        'title' => $this->t('Git Info Report - Assign the user www-data an SSH key and add it to the remote repo - Drupal 8 guide on Drupal.org'),
        'target' => '_blank',
      ],
    ]))->toString();
    $description = $this->t("For example <em>'/var/www/html'</em> to monitor all projects in that directory. One directory per line. The <strong>absolute path</strong> from system root must be used, starting with a slash ( / ).<br />Alternatively, @use_global (recommended on a server). Any project that contains directories that are under Git version control can be added, also those running in a Virtual Machine and non-Drupal projects.<br />If fetching from private repositories is desired, @instructions.", ['@use_global' => $useGlobal, '@instructions' => $instructions]);
    $form['directories'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Directories'),
    ];
    $form['directories']['extra_directories'] = [
      '#type' => 'textarea',
      '#title' => $this->t('To monitor outside the current project root'),
      '#description' => $description,
      '#default_value' => str_replace(',', PHP_EOL, $config->get('extra_directories')),
      '#disabled' => $config->hasOverrides(),
    ];
    $form['directories']['aggregate_directories'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Aggregate items in ignored directories to show only an item count per directory'),
      '#description' => $this->t("To save vertical space in the report. One directory path per line. The <strong>relative path</strong> from the project root must be used. Read the project's <em>.gitignore</em> file for inspiration or just see which directories result in long lists.<br />Leave empty to reset to the default."),
      '#default_value' => $config->get('aggregate_directories'),
    ];
    $form['render'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Render options'),
    ];
    $form['render']['number_commit_messages'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of latest commit messages to show per Git repo'),
      '#description' => $this->t('0 = none. -1 = unlimited (minus one). Default: <em>2</em>'),
      '#default_value' => $config->get('number_commit_messages'),
      '#min' => -1,
      '#max' => 99,
      '#size' => 2,
      '#maxlength' => 2,
    ];
    $form['render']['threshold_list'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of minimum items in a list to show it in a collapsed state (closed)'),
      '#description' => $this->t('Default: <em>10</em>'),
      '#default_value' => $config->get('threshold_list'),
      '#min' => 1,
      '#max' => 99,
      '#size' => 2,
      '#maxlength' => 2,
    ];
    $form['render']['list_always_collabsible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("<strong>Lists are always collapsible</strong>"),
      '#description' => $this->t("Lists under the above minimum will show collapsible in an uncollapsed state (open). This provides a uniform design for all lists."),
      '#default_value' => $config->get('list_always_collabsible'),
    ];
    $form['render']['fuzzy_timestamp_granularity'] = [
      '#type' => 'number',
      '#title' => $this->t("Fuzzy timestamp granularity (<em>'time ago'</em> date format)"),
      '#description' => $this->t("Examples of output with the currently defined granularity: <em><strong>@sample_old ago</strong></em> (years back), <em><strong>@sample_recent ago</strong></em> (recent).<br />The minimum of '1' results for example in <em>'2 weeks ago'</em>. A value of '5' (or higher) in <em>'2 weeks 4 days 18 hours 10 min 15 sec ago'</em>.<br />A low granularity is understood at a glance although is less accurate. The exact date and time are shown on mouse-over anyway (see below). Default: <em>1</em>.<br />NOTE: Granularity is not the same as the number of units. Following units in the hierarchy will not get skipped. For example <em>'5 months 19 hours'</em> will always be shown as <em>'5 months'</em>, independent from the granularity setting. After a week it will then turn into <em>'5 months 1 week'</em>.", [
        '@sample_old' => $this->dateFormatter->formatInterval(81950198, $config->get('fuzzy_timestamp_granularity')),
        '@sample_recent' => $this->dateFormatter->formatInterval(76477, $config->get('fuzzy_timestamp_granularity')),
      ]),
      '#default_value' => $config->get('fuzzy_timestamp_granularity'),
      '#min' => 1,
      '#max' => 9,
      '#size' => 1,
      '#maxlength' => 1,
    ];
    $phpManual = Link::fromTextAndUrl($this->t('PHP manual'), Url::fromUri('https://www.php.net/manual/en/function.date.php', [
      'attributes' => [
        'title' => $this->t('PHP: date - Manual'),
        'target' => '_blank',
      ],
    ]))->toString();
    $strToTime = Link::fromTextAndUrl($this->t('strtotime'), Url::fromUri('https://www.php.net/manual/en/function.strtotime.php', [
      'attributes' => [
        'title' => $this->t('PHP: strtotime - Manual'),
        'target' => '_blank',
      ],
    ]))->toString();
    $form['render']['exact_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The exact date and time format that is shown as a tooltip on the fuzzy timestamp'),
      '#description' => $this->t("Example output of the current time: <em><strong>@sample</strong></em><br />A user-defined date format. See the @php_manual for available options. <strong>Leave empty to reset to the default</strong>: <em>Y-m-d H:i:s O</em><br />Validated by passing the escaped format string through the PHP function @strtotime to ensure we get a sensible date.", [
        '@sample' => date($config->get('exact_date_format')),
        '@php_manual' => $phpManual,
        '@strtotime' => $strToTime,
      ]),
      '#default_value' => $config->get('exact_date_format'),
      '#size' => 30,
      '#maxlength' => 60,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $extras = array_map('trim', explode(PHP_EOL, $form_state->getValue('extra_directories')));
    foreach ($extras as $extra) {
      $exists = shell_exec('[ -d ' . escapeshellcmd('/' . trim($extra, " /\t\n\r")) . ' ] && echo "1"');
      if (!$exists) {
        $form_state->setErrorByName('extra_directories', $this->t('Directory %directory does not exist.', ['%directory' => '/' . trim($extra, " /\t\n\r")]));
      }
    }
    $aggregates = array_map('trim', explode(PHP_EOL, $form_state->getValue('aggregate_directories')));
    foreach ($aggregates as $aggregate) {
      if (!preg_match('/^[\w|\/]+$/', $aggregate) && !empty(trim($form_state->getValue('aggregate_directories')))) {
        $form_state->setErrorByName('aggregate_directories', $this->t('Directory %directory is invalid.', ['%directory' => trim($aggregate, " /\t\n\r")]));
      }
    }
    $validDate = (bool) strtotime(date(preg_replace('/\\\\\w+/', '', $form_state->getValue('exact_date_format'))));
    if (!$validDate && !empty($form_state->getValue('exact_date_format'))) {
      $form_state->setErrorByName('exact_date_format', $this->t('Invalid format.'));
    }
    $allowGit = Link::fromTextAndUrl($this->t("allow the user <strong>%user</strong> to perform <em>git fetch</em>", ['%user' => shell_exec('whoami')]), Url::fromUri('https://www.drupal.org/node/3142770', [
      'attributes' => [
        'title' => $this->t('Drupal guide on Drupal.org'),
        'target' => '_blank',
      ],
    ]))->toString();
    if ($form_state->getValue('connect') === 'pull') {
      $this->messenger->addMessage($this->t("When selecting to connect to the remote, a simple file permission change is required to $allowGit.", ['@allow_git' => $allowGit]), 'warning');
    }
    if ($form_state->getValue('head_detached')) {
      $this->messenger->addMessage($this->t("When selecting to fix a 'HEAD detached' state, a simple file permission change is required to $allowGit.", ['@allow_git' => $allowGit]), 'warning');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $extraDirectories = [];
    $extras = array_filter(explode(PHP_EOL, trim($form_state->getValue('extra_directories'))));
    foreach ($extras as $extra) {
      $extraDirectories[] = '/' . trim($extra, " /\t\n\r");
    }
    $extraDirectories = implode(',', $extraDirectories);
    $aggregateDirectories = [];
    $aggregates = empty(trim($form_state->getValue('aggregate_directories'))) ? "vendor\r\nweb/modules/contrib\r\ndocroot/modules/contrib" : $form_state->getValue('aggregate_directories');
    $aggregates = explode(PHP_EOL, $aggregates);
    foreach ($aggregates as $aggregate) {
      $aggregateDirectories[] = trim($aggregate, " /\t\n\r");
    }
    $aggregateDirectories = implode(PHP_EOL, $aggregateDirectories);
    $numberCommitMessages = $form_state->getValue('number_commit_messages');
    $numberCommitMessages = empty($numberCommitMessages) && (string) $numberCommitMessages !== '0' ? 2 : $numberCommitMessages;
    $thresholdList = $form_state->getValue('threshold_list');
    $thresholdList = empty($thresholdList) && (string) $thresholdList !== '0' ? 10 : $thresholdList;
    $fuzzyTimestampGranularity = $form_state->getValue('fuzzy_timestamp_granularity');
    $fuzzyTimestampGranularity = empty($fuzzyTimestampGranularity) ? 1 : $fuzzyTimestampGranularity;
    $exactDateFormat = trim($form_state->getValue('exact_date_format'));
    $exactDateFormat = empty($exactDateFormat) ? 'Y-m-d H:i:s O' : $exactDateFormat;
    $this->config('gitinfo.settings')
      ->set('interval', $form_state->getValue('interval'))
      ->set('cron_user', $form_state->getValue('cron_user'))
      ->set('cron_pass', $form_state->getValue('cron_pass'))
      ->set('connect', $form_state->getValue('connect'))
      ->set('head_detached', $form_state->getValue('head_detached'))
      ->set('list_always_collabsible', $form_state->getValue('list_always_collabsible'))
      ->set('extra_directories', $extraDirectories)
      ->set('aggregate_directories', $aggregateDirectories)
      ->set('number_commit_messages', $numberCommitMessages)
      ->set('threshold_list', $thresholdList)
      ->set('fuzzy_timestamp_granularity', $fuzzyTimestampGranularity)
      ->set('exact_date_format', $exactDateFormat)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
